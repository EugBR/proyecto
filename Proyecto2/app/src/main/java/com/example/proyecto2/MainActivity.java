package com.example.proyecto2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final int PERMIT = 1;

    EditText edUser, edPwd;
    Button btNext;
    TextView tvNew, tvIdioma, tvDifuminar;
    String user, pwd, pwd2;
    ImageView imgLogo;
    String idioma = "espanol";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        getPermit();

        tvNew = findViewById(R.id.tvNew);
        edUser = findViewById(R.id.edUser);
        edPwd = findViewById(R.id.edPwd);
        btNext = findViewById(R.id.btNext);
        tvIdioma = findViewById(R.id.tvIdioma);
        tvDifuminar = findViewById(R.id.tvDifuminar);
        imgLogo = findViewById(R.id.imgLogo);
        tvDifuminar.setVisibility(View.GONE);


    }


    public void next(View view) {
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(MainActivity.this,
                "BaseDeDatos", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        String usuario = edUser.getText().toString();
        String password = edPwd.getText().toString();
        Cursor fila = null;
        Cursor fila1 = null;
        String traduccion = "";
        try {
            fila = BaseDeDatos.rawQuery("select contrasena from usuarios where nombre = '"
                    + usuario + "'", null);
            fila.moveToFirst();
        } catch (Exception e) {

        }
        try {
            fila = BaseDeDatos.rawQuery("select contrasena from usuarios where nombre = '"
                    + usuario + "'", null);
            fila.moveToFirst();
            pwd2 = fila.getString(0);
            if (password.matches(pwd2)) {
                fila1 = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 12;", null);
                fila1.moveToFirst();
                traduccion = fila1.getString(0);
                Toast.makeText(MainActivity.this, "" + traduccion,
                        Toast.LENGTH_SHORT).show();

                fila = BaseDeDatos.rawQuery("select id from usuarios where nombre = '"
                        + usuario + "'", null);
                fila.moveToFirst();

                String identificador = fila.getString(0);
                Intent intencion = new Intent(this, Menu.class);
                intencion.putExtra("identificador", "" + identificador);
                intencion.putExtra("idioma", "" + idioma);
                startActivity(intencion);
            } else {
                fila1 = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 13;", null);
                fila1.moveToFirst();
                traduccion = fila1.getString(0);
                Toast.makeText(MainActivity.this, "" + traduccion,
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            fila1 = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 14;", null);
            fila1.moveToFirst();
            traduccion = fila1.getString(0);
            Toast.makeText(MainActivity.this, "" + traduccion,
                    Toast.LENGTH_SHORT).show();
        }


        BaseDeDatos.close();

    }

    public void newUser(View view) {
        tvDifuminar.setAlpha(1);
        tvDifuminar.setVisibility(View.VISIBLE);
        difuminar difuminar = new difuminar();
        difuminar.start();
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();

        View view1 = inflater.inflate(R.layout.dialog_new_user, null);

        builder.setView(view1);
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                tvDifuminar.setVisibility(View.GONE);
            }
        });


        EditText editTextTextPersonName = view1.findViewById(R.id.editTextTextPersonName);
        EditText editTextTextPassword = view1.findViewById(R.id.editTextTextPassword);
        EditText editTextTextPassword2 = view1.findViewById(R.id.editTextTextPassword2);
        Button btRegistrar = view1.findViewById(R.id.btRegistrar);

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(MainActivity.this,
                "BaseDeDatos", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        String usuario = edUser.getText().toString();
        String password = edPwd.getText().toString();
        Cursor fila = null;
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 2;", null);
        fila.moveToFirst();
        editTextTextPassword.setHint(fila.getString(0));
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 6;", null);
        fila.moveToFirst();
        editTextTextPassword2.setHint(fila.getString(0));
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 5;", null);
        fila.moveToFirst();
        editTextTextPersonName.setHint(fila.getString(0));
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 7;", null);
        fila.moveToFirst();
        btRegistrar.setText(fila.getString(0));


        btRegistrar.setClickable(true);
        btRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = editTextTextPersonName.getText().toString();
                pwd = editTextTextPassword.getText().toString();
                pwd2 = editTextTextPassword2.getText().toString();
                boolean rep = false;
                // 0 letra minuscula | 1 letra mayuscula | 2 numero
                boolean secure[] = new boolean[3];
                for (int i = 0; i < 3; i++) {
                    secure[i] = false;
                }

                AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper
                        (MainActivity.this, "BaseDeDatos", null, 1);
                SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
                String aa = "";
                Cursor fila = null;
                Cursor fila1 = null;
                try {
                    fila = BaseDeDatos.rawQuery
                            ("select nombre from usuarios where nombre = '" + user + "'"
                                    , null);
                    fila.moveToFirst();
                    aa = fila.getString(0);
                    rep = true;
                } catch (Exception e) {
                }


                //comprobar seguridad de la contraseña
                for (int i = 0; i < pwd.length(); i++) {
                    char a = pwd.charAt(i);
                    if (Character.isDigit(a) && secure[2] == false) {
                        secure[2] = true;
                    } else if (Character.isUpperCase(a) && secure[1] == false) {
                        secure[1] = true;
                    } else if (Character.isLowerCase(a) && secure[0] == false) {
                        secure[0] = true;
                    }

                }

                String traduccion = "";
                if (user.matches("") || pwd.matches("") || pwd2.matches("")) {
                    //huecos incompletos
                    fila1 = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 15;", null);
                    fila1.moveToFirst();
                    traduccion = fila1.getString(0);
                    Toast.makeText(MainActivity.this,
                            "" + traduccion, Toast.LENGTH_SHORT).show();
                } else if (pwd.matches(pwd2) == false) {
                    //contraseñas diferentes
                    fila1 = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 16;", null);
                    fila1.moveToFirst();
                    traduccion = fila1.getString(0);
                    Toast.makeText(MainActivity.this,
                            "" + traduccion, Toast.LENGTH_SHORT).show();
                } else if (pwd.length() < 8) {
                    //tamaño contraseña
                    fila1 = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 17;", null);
                    fila1.moveToFirst();
                    traduccion = fila1.getString(0);
                    Toast.makeText(MainActivity.this,
                            "" + traduccion,
                            Toast.LENGTH_SHORT).show();
                } else if (secure[0] != true || secure[1] != true || secure[2] != true) {
                    //baja seguridad
                    fila1 = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 18;", null);
                    fila1.moveToFirst();
                    traduccion = fila1.getString(0);
                    Toast.makeText(MainActivity.this,
                            "" + traduccion, Toast.LENGTH_SHORT).show();

                } else if (rep == true) {
                    fila1 = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 19;", null);
                    fila1.moveToFirst();
                    traduccion = fila1.getString(0);
                    Toast.makeText(MainActivity.this, "" + traduccion, Toast.LENGTH_SHORT).show();

                } else {
                    //CORRECTO
                    admin = new AdminSQLiteOpenHelper(MainActivity.this,
                            "BaseDeDatos", null, 1);
                    BaseDeDatos = admin.getWritableDatabase();
                    ContentValues registro = new ContentValues();
                    registro.put("nombre", user);
                    registro.put("contrasena", pwd);


                    BaseDeDatos.insert("usuarios", null, registro);
                    fila1 = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 20;", null);
                    fila1.moveToFirst();
                    traduccion = fila1.getString(0);

                    Toast.makeText(MainActivity.this, "" + traduccion,
                            Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    BaseDeDatos.close();
                }
                BaseDeDatos.close();
            }
        });


    }

    private void getPermit() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    AlertDialog.Builder alertb = new AlertDialog.Builder(MainActivity.this);
                    alertb.setTitle("Acepta los permisos");
                    alertb.setMessage("Para que la aplicacion funcione correctamente, es necesario que aceptes los permisos de almacenamiento");
                    alertb.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMIT);
                        }
                    });

                    alertb.setNegativeButton("cancel", null);
                    AlertDialog alertDialog = alertb.create();
                    alertDialog.show();

                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMIT);
                }

            }

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            cerrarAplicacion();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void cerrarAplicacion() {
        new AlertDialog.Builder(this)
                .setIcon(R.mipmap.logo1)
                .setTitle("¿Estas seguro de que quieres cerrar la aplicacion?")
                .setCancelable(false)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {// un listener que al pulsar, cierre la aplicacion
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        android.os.Process.killProcess(android.os.Process.myPid()); //Su funcion es algo similar a lo que se llama cuando se presiona el botón "Forzar Detención" o "Administrar aplicaciones", lo cuál mata la aplicación
                        //finish(); Si solo quiere mandar la aplicación a segundo plano
                    }
                }).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMIT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    ;
            }
        }
    }

    public void cambiarIdioma(View view) {

        if (idioma.matches("espanol")) {
            idioma = "ingles";
        } else {
            idioma = "espanol";
        }


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(MainActivity.this,
                "BaseDeDatos", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        String usuario = edUser.getText().toString();
        String password = edPwd.getText().toString();
        Cursor fila = null;
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 1;", null);
        fila.moveToFirst();
        edUser.setHint(fila.getString(0));
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 2;", null);
        fila.moveToFirst();
        edPwd.setHint(fila.getString(0));
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 3;", null);
        fila.moveToFirst();
        btNext.setText(fila.getString(0));
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 4;", null);
        fila.moveToFirst();
        tvNew.setText(fila.getString(0));


    }

    public class difuminar extends Thread {
        public void run() {
            for (int i = 0; i < 10; i++) {
                try {
                    sleep(50);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvDifuminar.setAlpha((float) (tvDifuminar.getAlpha() - 0.05));
                        }
                    });
                } catch (Exception e) {

                }

            }
        }
    }


}