package com.example.proyecto2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {
    public AdminSQLiteOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase BaseDeDatos) {
        BaseDeDatos.execSQL("create table usuarios(ID integer primary key autoincrement, nombre text, contrasena text);");
        BaseDeDatos.execSQL("create table playlist(ID integer primary key autoincrement, idUsuario integer REFERENCES ID(usuarios), nombrePlaylist TEXT, cancion TEXT, duracion integer);");
        BaseDeDatos.execSQL("CREATE TABLE idiomas(id integer primary key, espanol TEXT, ingles TEXT);");
//        BaseDeDatos.execSQL("INSERT INTO usuarios (nombre, contrasena) VALUES('Eugenio','123456Aa');");

        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(1,'Usuario','User');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(2,'Contraseña','Password');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(3,'Siguiente','Next');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(4,'Crear un Nuevo Usuario','Create a New User');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(5,'Nombre de Usuario','User Name');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(6,'Repita la Contraseña','Repeat the Password');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(7,'Registrar','Sign Up');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(8,'Canciones','Songs');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(9,'Seleccione una cancion','Select a song');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(10,'Aceptar','Accept');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(11,'Crear Playlist','Create Playlist');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(12,'Inicio de sesion correcto','Succesful login');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(13,'Contraseña Incorrecta','Incorrect Password');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(14,'Esta cuenta no existe','This account does not exist');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(15,'Debes rellenar todos los huecos','You mus fill all the holes');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(16,'Las contraseñas no coinciden','The passwords does not match');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(17,'La contraseña debe tener al menos 8 caracteres','The passwords must have at least 8 characters');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(18,'La contraseña debe contener al menos una mayuscula, minuscula y un digito','The password must contain at least one capital letter, small and a digit');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(19,'Este nombre de usuario ya existe','This username already exists');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(20,'Cuenta creada','Account created');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(21,'No puedes dejar el nombre vacio','You can not leave the name empty');");
        BaseDeDatos.execSQL("INSERT INTO idiomas (id, espanol, ingles) VALUES(22,'Este nombre ya existe','This name already exists');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
