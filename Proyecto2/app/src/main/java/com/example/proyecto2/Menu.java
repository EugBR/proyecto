package com.example.proyecto2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static java.lang.Thread.sleep;

public class Menu extends AppCompatActivity {

    private int count;
    private int num;
    private int identificador;
    private int TRACK_Column;
    private int _ID_Column;
    private int DATA_Column;
    private int YEAR_Column;
    private int DURATION_Column, ALBUM_ID_Column, ALBUM_Column, ARTIST_Column;
    private String idioma = "espanol";
    ArrayAdapter<String> adapter;
    TextView title, artist, duration, tvCancion, tvLista;
    private String[] audioLista, artistLista, arrPath, durationLista;
    ListView listaLista;
    SeekBar seekBarTime;
    Boolean random = false, loop = false;


    ImageView imgPlay, imgNextL, imgNextR, imgRand, imgLoop;
    MediaPlayer mediaPlayer = new MediaPlayer();
    Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getSupportActionBar().hide();
        Bundle datos = getIntent().getExtras();
        identificador = Integer.valueOf(datos.getString("identificador"));
        idioma = datos.getString("idioma");


        imgNextL = findViewById(R.id.imageNextL);
        imgNextR = findViewById(R.id.imageNextR);
        imgPlay = findViewById(R.id.imagePlay);
        imgLoop = findViewById(R.id.imageViewRepeat);
        imgRand = findViewById(R.id.imageViewRandom);
        tvCancion = findViewById(R.id.textView_Cancion);
        seekBarTime = findViewById(R.id.seekBarTime);
        listaLista = findViewById(R.id.listView_Lista);
        tvLista = findViewById(R.id.textViewLista);



        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Menu.this,
                "BaseDeDatos", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        Cursor fila = null;
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 8;", null);
        fila.moveToFirst();
        tvLista.setText(fila.getString(0));
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 9;", null);
        fila.moveToFirst();
        tvCancion.setText(fila.getString(0));


        BaseDeDatos.close();


        audioCursorLista();


        AudioAdapter audioAdapter = new AudioAdapter();

        listaLista.setAdapter(audioAdapter);


        seekBarTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBar.getProgress());
            }
        });


    }


    private void audioCursorLista() {

        String[] information = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.TRACK,
                MediaStore.Audio.Media.YEAR,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_KEY,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.TITLE_KEY,
                MediaStore.Audio.Media.ARTIST_ID,
                MediaStore.Audio.Media.ARTIST
        };
        final String orderBy = MediaStore.Audio.Media._ID;
        Cursor audioCursor = getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, information, null,
                null, orderBy);
        count = audioCursor.getCount();
        audioLista = new String[count];
        artistLista = new String[count];
        arrPath = new String[count];
        durationLista = new String[count];

        _ID_Column = audioCursor.getColumnIndex((MediaStore.Audio.Media._ID));
        DATA_Column = audioCursor.getColumnIndex((MediaStore.Audio.Media.DATA));
        YEAR_Column = audioCursor.getColumnIndex((MediaStore.Audio.Media.YEAR));
        DURATION_Column = audioCursor.getColumnIndex((MediaStore.Audio.Media.DURATION));
        ALBUM_ID_Column = audioCursor.getColumnIndex((MediaStore.Audio.Media.ALBUM_ID));
        ALBUM_Column = audioCursor.getColumnIndex((MediaStore.Audio.Media.ALBUM));
        TRACK_Column = audioCursor.getColumnIndex((MediaStore.Audio.Media.TITLE));
        ARTIST_Column = audioCursor.getColumnIndex((MediaStore.Audio.Media.ARTIST));


        while (audioCursor.moveToNext()) {

            audioLista[getNum()] = audioCursor.getString(TRACK_Column);
            artistLista[getNum()] = audioCursor.getString(ARTIST_Column);
            durationLista[getNum()] = audioCursor.getString(DURATION_Column);
            arrPath[getNum()] = audioCursor.getString(DATA_Column);
            setNum(getNum() + 1);
        }
        //max = getNum();

        audioCursor.close();
    }


    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }


    public class AudioAdapter extends BaseAdapter {
        private LayoutInflater inflater;

        public AudioAdapter() {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public Object getItem(int position) {
            return getNum();
        }

        @Override
        public long getItemId(int position) {
            return getNum();
        }

        @Override
        public View getView(int i, View view, ViewGroup parent) {

            view = inflater.inflate(R.layout.rows, null);
            title = (TextView) view.findViewById(R.id.textView_Title);
            artist = (TextView) view.findViewById(R.id.textView_Artis);
            duration = (TextView) view.findViewById(R.id.textView_Duration);

            title.setId(i);
            artist.setId(i);
            duration.setId(i);


            title.setText(audioLista[i]);
            artist.setText(artistLista[i]);
            long tmp = Integer.parseInt(durationLista[i]);
            duration.setText(convertDuration(tmp));

            listaLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                    //cada vez que se seleccina una cancion se genera

                    setNum(i);

                    playAudio();
                }
            });

            listaLista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    mostrarDialogo(position);
                    return false;
                }
            });


            return view;

        }


    }


    public String convertDuration(long duration) {
        String out = null;
        long hours = 0;
        try {
            hours = (duration / 3600000);

        } catch (Exception e) {
            e.printStackTrace();
            return out;
        }

        long remaining_minutes = (duration - (hours * 3600000)) / 60000;
        String minutes = String.valueOf(remaining_minutes);
        if (minutes.equals(0)) {
            minutes = "0";
        }
        long remaining_seconds = (duration - (hours * 3600000) - (remaining_minutes * 60000));
        String seconds = String.valueOf(remaining_seconds);
        if (seconds.length() < 2) {
            seconds = "00";

        } else {
            seconds = seconds.substring(0, 2);
        }

        if (hours > 0) {
            out = hours + ":" + minutes + ":" + seconds;
        } else {
            out = minutes + ":" + seconds;
        }

        return out;
    }


    private void playAudio() {

        tvCancion.setText((audioLista[getNum()]));
        mediaPlayer.stop();
        mediaPlayer.reset();


        handler = new Handler();
        try {
            mediaPlayer.setDataSource(arrPath[getNum()]);

            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer player) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        mediaPlayer.start();
                        seekBarTime.setMax(mediaPlayer.getDuration());


                        if (mediaPlayer.isPlaying()) {
                            imgPlay.setImageResource(R.mipmap.pause);
                        } else {
                            imgPlay.setImageResource(R.mipmap.play);
                        }

                    }
                });
                //updateSeekBar.start();
                UpdateSeekBar up = new UpdateSeekBar();
                up.start();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                if (random) {
                    boolean otra = true;
                    while (otra) {
                        Random r = new Random();
                        int bla = r.nextInt(audioLista.length);
                        if (bla == getNum()) {
                            otra = true;
                        } else {
                            otra = false;
                            setNum(bla);
                            playAudio();
                            seekBarTime.setMax(mediaPlayer.getDuration());

                        }

                    }

                } else {
                    int bla = getNum() + 1;
                    if (bla > audioLista.length - 1) {
                        bla = 0;
                    }
                    setNum(bla);
                    playAudio();
                    seekBarTime.setMax(mediaPlayer.getDuration());
                }


            }
        });


        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()) {
                    imgPlay.setImageResource(R.mipmap.play);
                    mediaPlayer.pause();
                } else {
                    imgPlay.setImageResource(R.mipmap.pause);
                    mediaPlayer.start();
                }
            }
        });

    }

    public void pasarPlaylist(View view) {
        Intent intencion;

        switch (view.getId()) {

            case R.id.textViewPlaylist:
                mediaPlayer.stop();

                intencion = new Intent(this, MenuPlaylist.class);
                intencion.putExtra("identificador", "" + identificador);
                intencion.putExtra("idioma", "" + idioma);
                startActivity(intencion);


                break;

            case R.id.textViewLista:
                mediaPlayer.stop();

                intencion = new Intent(this, Menu.class);
                intencion.putExtra("identificador", "" + identificador);
                intencion.putExtra("idioma", "" + idioma);
                startActivity(intencion);


                break;


            case R.id.textViewPodcast:
                mediaPlayer.stop();

                intencion = new Intent(this, MenuPodcast.class);
                intencion.putExtra("identificador", "" + identificador);
                intencion.putExtra("idioma", "" + idioma);
                startActivity(intencion);


                break;
        }
    }


    public void next(View view) {
        if (random) {
            boolean otra = true;
            while (otra) {
                Random r = new Random();
                int bla = r.nextInt(audioLista.length);
                if (bla == getNum()) {
                    otra = true;
                } else {
                    otra = false;
                    setNum(bla);
                    playAudio();
                    seekBarTime.setMax(mediaPlayer.getDuration());

                }

            }

        } else {
            int bla = getNum() + 1;
            if (bla > audioLista.length - 1) {
                bla = 0;
            }
            setNum(bla);
            playAudio();
            seekBarTime.setMax(mediaPlayer.getDuration());
        }
    }


    public void back(View view) {
        int bla = getNum() - 1;
        if (bla < 0) {
            bla = 0;
        }


        setNum(bla);
        playAudio();
        seekBarTime.setMax(mediaPlayer.getDuration());
    }

    public void repeat(View view) {
        if (!loop) {
            imgLoop.setImageResource(R.mipmap.loop);
            loop = true;
        } else {
            imgLoop.setImageResource(R.mipmap.loop1);
            loop = false;
        }
    }

    public void random(View view) {
        if (!random) {
            imgRand.setImageResource(R.mipmap.random);
            random = true;
        } else {
            imgRand.setImageResource(R.mipmap.random1);
            random = false;
        }
    }


    public class UpdateSeekBar extends Thread {
        public void run() {
            int currentPosition = 0;

            while (mediaPlayer.isPlaying()) {

                try {
                    sleep(500);
                    currentPosition = mediaPlayer.getCurrentPosition();
                    seekBarTime.setProgress(currentPosition);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void mostrarDialogo(int position) {


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        View row = getLayoutInflater().inflate(R.layout.dialog_playlist, null);

        ListView lvPlaylist = row.findViewById(R.id.lvPlaylists);
        EditText edNuevaPlaylist = row.findViewById(R.id.edCrearPlayList);
        Button btOk = row.findViewById(R.id.btOk);

        ArrayList<String> playslists = new ArrayList<>();

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Menu.this,
                "BaseDeDatos", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        Cursor fila = null;
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 10;", null);
        fila.moveToFirst();
        btOk.setText(fila.getString(0));
        fila = BaseDeDatos.rawQuery("SELECT " + idioma + " FROM idiomas WHERE id = 11;", null);
        fila.moveToFirst();
        playslists.add("" + fila.getString(0));
        fila = BaseDeDatos.rawQuery("SELECT distinct p.nombrePlaylist FROM playlist p INNER JOIN usuarios u ON p.idUsuario=u.id WHERE (u.id = '" + identificador + "');", null);
        fila.moveToFirst();

        alertDialog.setView(row);
        AlertDialog dialog = alertDialog.create();
        dialog.show();

        int contar = fila.getCount();

        while (fila.getPosition() < fila.getCount()) {
            playslists.add(fila.getString(0));
            fila.moveToNext();

        }


        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, playslists);
        lvPlaylist.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        lvPlaylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                if (i == 0) {//crear playlist
                    lvPlaylist.setVisibility(View.GONE);
                    btOk.setVisibility(View.VISIBLE);
                    edNuevaPlaylist.setVisibility(View.VISIBLE);

                    btOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (comprobarPlaylistsRepetidas(edNuevaPlaylist.getText().toString(), playslists)) {
                                AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper
                                        (Menu.this, "BaseDeDatos", null, 1);
                                SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
                                admin = new AdminSQLiteOpenHelper(Menu.this,
                                        "BaseDeDatos", null, 1);
                                BaseDeDatos = admin.getWritableDatabase();
                                ContentValues registro = new ContentValues();
                                registro.put("idUsuario", identificador);
                                registro.put("nombrePlaylist", edNuevaPlaylist.getText().toString());
                                registro.put("cancion", audioLista[position]);
                                registro.put("duracion", Integer.parseInt(durationLista[position]));


                                BaseDeDatos.insert("playlist", null, registro);
                                BaseDeDatos.close();
                                dialog.dismiss();
                            }


                        }
                    });


                } else {//añadir a playlist

                    AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper
                            (Menu.this, "BaseDeDatos", null, 1);
                    SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
                    admin = new AdminSQLiteOpenHelper(Menu.this,
                            "BaseDeDatos", null, 1);
                    BaseDeDatos = admin.getWritableDatabase();
                    ContentValues registro = new ContentValues();
                    registro.put("idUsuario", identificador);
                    registro.put("nombrePlaylist", playslists.get(i));
                    registro.put("cancion", audioLista[position]);
                    registro.put("duracion", Integer.parseInt(durationLista[position]));


                    BaseDeDatos.insert("playlist", null, registro);
                    BaseDeDatos.close();
                    dialog.dismiss();

                }

            }
        });


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            cerrarAplicacion();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void cerrarAplicacion() {
        new AlertDialog.Builder(this)
                .setIcon(R.mipmap.logo1)
                .setTitle("¿Estas seguro de que quieres cerrar la aplicacion?")
                .setCancelable(false)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {// un listener que al pulsar, cierre la aplicacion
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mediaPlayer.stop();
                        android.os.Process.killProcess(android.os.Process.myPid()); //Su funcion es algo similar a lo que se llama cuando se presiona el botón "Forzar Detención" o "Administrar aplicaciones", lo cuál mata la aplicación
                        //finish(); Si solo quiere mandar la aplicación a segundo plano
                    }
                }).show();
    }


    public boolean comprobarPlaylistsRepetidas(String a, ArrayList<String> playslists) {
        boolean resultado = false;
        int contador = 0;

        if (a.matches("")) {
            resultado = false;
            Toast.makeText(Menu.this, "No puedes dejar el nombre vacio", Toast.LENGTH_SHORT).show();
        } else {
            contador++;

        }

        for (int i = 0; i < playslists.size(); i++) {
            if (a.matches(playslists.get(i))) {
                contador = contador + 100;
                Toast.makeText(Menu.this, "Este nombre ya existe", Toast.LENGTH_SHORT).show();
            } else {
                contador++;

            }
        }


        if (contador == (1 + playslists.size())) {
            resultado = true;
        }


        return resultado;
    }


}



